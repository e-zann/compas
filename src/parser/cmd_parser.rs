#![allow(unused)]



/*

    Important ASCII Codes 

    0x7c    ->    '|' Pipe()
    0x24    ->    '$' Init  CMD-Substitution
    0x28    ->    '(' Begin CMD-Substitution
    0x29    ->    ')' End   CMD-Substitution
    0x3e    ->    '>' Redirect Right
    0x3c    ->    '>' Redirect Left
    0x22    ->    '"" Double Quotes

*/


#[derive(Debug,Clone)]
pub struct Compas {
    pub cmd    : Vec<u8>,
    pub params : Option<Vec<Vec<u8>>>,
}

#[derive(Debug,Clone)]
pub struct CompasTable {
    pub commands : Vec<Compas>,
    pub pipe     : bool,
}



impl Compas {

    
    fn raw_to_compas( cmd_byte_line: &Vec<Vec<u8>> ) -> Compas {

        if cmd_byte_line.len() > 1 {
            Compas {
                cmd    : cmd_byte_line[0].clone(),
                params : Some(cmd_byte_line[1..].to_vec()),
            }
        } else {
            Compas {
                cmd    : cmd_byte_line[0].clone(),
                params : None,
            }
        } 
    }


    pub fn cmd_from_bytes( byte_line: &[u8] ) -> CompasTable {


        let mut compas_table                 = CompasTable { commands : Vec::new(), pipe : false, };
        let mut cmd_token     : Vec<u8>      = Vec::new();
        let mut cmd_vec       : Vec<Vec<u8>> = Vec::new();
        let mut quoted        : bool         = false;
        let mut piped         : bool         = false;

        for byte in byte_line {
            match byte  {

                // pipe
                0x7c => {

                    compas_table.pipe = true;

                    if !cmd_token.is_empty() {
                            cmd_vec.push(cmd_token.clone());    
                            compas_table.commands.push(
                            Self::raw_to_compas(&cmd_vec)
                            .clone());
                        cmd_token.clear();
                        cmd_vec.clear();
                        
                    }
                },

                // double quoted
                0x22 => {
                    if quoted == false {
                        quoted = true;
                    } else {
                        quoted = false;
                    }
                },

                // space
                0x20 => {
                    if quoted == true {
                        cmd_token.push(*byte);
                    } else {
                        cmd_vec.push(cmd_token.clone());
                        compas_table.commands.push(
                            Self::raw_to_compas(&cmd_vec)
                            .clone());
                        cmd_token.clear();
                        cmd_vec.clear();
                    }
                },

                0x21 | 0x23 ..= 0x7b  => {
                    cmd_token.push(*byte);
          
                },

                _ => {},

            };
        }

        if !cmd_token.is_empty() {
            cmd_vec.push(cmd_token.clone());
            compas_table.commands.push(
                Self::raw_to_compas(&cmd_vec)
                .clone());
            cmd_token.clear();
            cmd_vec.clear();
        }

        compas_table

    }
}
