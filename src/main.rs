#![allow(unused)]
use std::io::Write;

use libcompas::cmd_parser::Compas;


fn main() {

    let raw = "cat file.txt | grep -i blub | echo \"testi test\" $PWD".to_string();
    let raw_bytes = raw.as_bytes();

    let parsed_raw = Compas::cmd_from_bytes(&raw_bytes);


    for c in parsed_raw.commands {
        std::io::stdout().write("\n".as_bytes());
        std::io::stdout().write( c.cmd.as_slice());
        std::io::stdout().write(" ".as_bytes());
        match &c.params {
            Some(p) => {
                for ps in p {
                    std::io::stdout().write(&ps.as_slice());
                    std::io::stdout().write(" ".as_bytes());
                }
            },
            None => {},
        }
    }
    
    print!("\r\n");
    std::io::stdout().flush().unwrap();

}
